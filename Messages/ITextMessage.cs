﻿using System;

namespace Messages
{
    public interface ITextMessage
    {
        string Text { get; set; }
        DateTime TimeStamp { get; set; }
    }
}