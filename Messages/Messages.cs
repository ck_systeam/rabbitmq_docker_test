﻿using System;

namespace Messages
{
    public class TextMessage : ITextMessage
    {
        public string Text { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
