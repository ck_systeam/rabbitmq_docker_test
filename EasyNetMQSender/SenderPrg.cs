﻿using EasyNetQ;
using Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyNetMQSender
{
    class SenderPrg
    {
        const string HostAddress = "192.168.99.100";
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=" + HostAddress))
            {
                var input = "";
                Console.WriteLine("Enter a message.");
                Console.WriteLine("> 'test:n' to send n messages");
                Console.WriteLine("> 'big:n' to send one message with n characters");
                Console.WriteLine("> 'rpc:<msg>' to send an Message and wait for response.");
                Console.WriteLine("> 'Quit' to quit.");
                while ((input = Console.ReadLine().ToLower()) != "quit")
                {
                    if (input.ToLower().StartsWith("test:"))
                    {
                        var n = Convert.ToInt64(input.Substring(5));
                        for (int i = 0; i < n; i++)
                        {
                            var msg = new TextMessage { Text = string.Format("{0,5}", i), TimeStamp = DateTime.Now };
                            bus.Publish<ITextMessage>(msg);
                        }
                    }
                    else if (input.ToLower().StartsWith("big:"))
                    {
                        var n = Convert.ToInt32(input.Substring(4));
                        var sb = new StringBuilder(n);

                        for (int i = 0; i < n; i++)
                        {
                            sb.Append(".");
                        }
                        var msg = new TextMessage { Text = string.Format("{0}, length={1}", sb, sb.Length), TimeStamp = DateTime.Now };
                        bus.Publish<ITextMessage>(msg);
                    }
                    else if (input.ToLower().StartsWith("rpc:"))
                    {
                        try
                        {
                            var msg = new TextMessage { Text = input.Substring(4), TimeStamp = DateTime.Now };
                            var response = bus.Request<ITextMessage, string>(msg);
                            Console.WriteLine("response: {0}", response);
                            Console.WriteLine("Queue delay = {0:F1} ms", (DateTime.Now - msg.TimeStamp).Ticks/10000.0);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("response: {0}", e.Message);
                            if (null != e.InnerException)
                            {
                                Console.WriteLine("response: {0}", e.InnerException.Message);
                            }
                        }
                    }
                    else
                    {
                        var msg = new TextMessage { Text = input, TimeStamp = DateTime.Now };
                        bus.Publish<ITextMessage>(msg);
                    }
                }
            }

        }
    }
}
