﻿using EasyNetQ;
using Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyNetMQListener
{
    class ListenerPrg
    {
        const string HostAddress = "192.168.99.100";

        static void Main(string[] args)
        {
            var subscriptionID = "A"; // Guid.NewGuid().ToString();
            using (var bus = RabbitHutch.CreateBus("host="+HostAddress))
            {
                Console.WriteLine("Connected to RabbitMQ?  {0}", bus.IsConnected);
               
                bus.Subscribe<ITextMessage>(subscriptionID, HandleTextMessage);
                bus.Respond<ITextMessage, string>(HandleRPCcall);

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }

        static void HandleTextMessage(ITextMessage textMessage)
        {
            var dtReceived = DateTime.Now;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Got message: {0}", textMessage.Text);
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            var dtShown = DateTime.Now;
            var tsDiff = dtShown - dtReceived;
            Console.WriteLine("sent: {0:HH:mm:ss.fff}, received: {1:HH:mm:ss.fff}, diff: {2:F1} ms", textMessage.TimeStamp, dtReceived, (dtReceived - textMessage.TimeStamp).Ticks / 10000.0);
            Console.WriteLine("Time to update console = {0:F1} ms", tsDiff.Ticks / 10000.0);
            Console.ResetColor();
        }

        private static string HandleRPCcall(ITextMessage arg)
        {
            Console.WriteLine("RPC Call '{0}' received", arg.Text);
            return string.Format("'{0}' received! This is the answer.", arg.Text);
        }

    }
}
