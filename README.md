# README #

This is an internal test project. It shows how to use [RabbitMQ](https://www.rabbitmq.com/) with [EasyNetQ](http://easynetq.com/) from C#.

## Preperation ##
You need a running rabbitMQ instance. The easiest way to get this, is using the official docker image. 

1. Download and install the Docker Toolbox:
[https://www.docker.com/products/docker-toolbox]

1. Start the Docker Console and download the rabbitMQ container with:
```
#!
docker pull rabbitmq:3-management
```
1. Run the docker instance with:
```
#!
docker run -d -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```
1. get the name and the IP Adress of the docker host
```
#!
docker-machine active
docker-machine IP <name from previous command, normally: 'default'>
```
1. set the IP Adress in the **Sender** and **Listener** project.

## Start testing ##
Now start the Sender and Listener and create some Messages in the Sender Window.

After some tests try the following steps:
2. stop the listener application
2. Enter some messages (more than 3) in the Sender Window
2. stop the sender application
2. wait 5 seconds
2. restart the listener

The messages are received even the Listener was not running when sending the messages.